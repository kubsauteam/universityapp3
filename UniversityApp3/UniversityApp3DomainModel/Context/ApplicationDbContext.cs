﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using UniversityApp3DomainModel.Models;
using UniversityApp3Web.Models;

namespace UniversityApp3DomainModel.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
        
        //public DbSet<Discipline> Disciplines { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


    }
}

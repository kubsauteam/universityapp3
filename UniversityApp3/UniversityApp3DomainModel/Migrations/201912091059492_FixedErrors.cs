namespace UniversityApp3DomainModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedErrors : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FacultyId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "FacultyId");
        }
    }
}

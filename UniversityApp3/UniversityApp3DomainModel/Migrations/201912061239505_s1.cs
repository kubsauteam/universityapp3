namespace UniversityApp3DomainModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class s1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "GroupId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "StudentId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "StudentId");
            DropColumn("dbo.AspNetUsers", "GroupId");
        }
    }
}

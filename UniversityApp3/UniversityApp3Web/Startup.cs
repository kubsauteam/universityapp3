﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UniversityApp3Web.Startup))]
namespace UniversityApp3Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityApp3DomainModel.Context;
using UniversityApp3DomainModel.Models;
using UniversityApp3Web;
using UniversityApp3Web.Models;

namespace UniversityApp2Web.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public AppRoleManager RoleManager
        {
            get
            {
                return Request.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }

        public UserManager<ApplicationUser> UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        // Факультеты
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Faculties()
        {

            var faculties = db.Faculties;

            ViewBag.Faculties = faculties;

            return View();

        }


        // Добавить факультет
        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Faculties(Faculty faculty)
        {
            db.Faculties.Add(faculty);
            db.SaveChanges();

            return RedirectToAction("Faculties");
        }

        [Authorize(Roles = "admin,faculty")]
        [HttpGet]
        public ActionResult Groups(int? facultyId)
        {
            if (User.IsInRole("admin"))
            {
                if (facultyId == null)
                {
                    var groups = db.Groups;

                    SelectList faculties = new SelectList
                                            (db.Faculties, "FacultyId", "FacultyName");

                    ViewBag.Groups = groups;
                    ViewBag.Faculties = faculties;

                    return View();
                }
                else
                {
                    var groups = db.Groups.Where(p => p.FacultyId == facultyId);

                    SelectList faculties = new SelectList
                                            (db.Faculties, "FacultyId", "FacultyName");

                    ViewBag.Groups = groups;
                    ViewBag.Faculties = faculties;

                    return View();
                }


                
            }

            if (User.IsInRole("faculty"))
            {
                var user = db.Users.Find(User.Identity.GetUserId());


                var groups = db.Groups.Where(p => p.FacultyId == user.FacultyId);
                SelectList faculties = new SelectList
                                        (db.Faculties, "FacultyId", "FacultyName");


                ViewBag.Groups = groups;
                ViewBag.Faculties = faculties;

                return View();
            }

            

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin,faculty")]
        public ActionResult Groups(Group group)
        {
            db.Groups.Add(group);
            db.SaveChanges();

            return RedirectToAction("Groups");
        }

        
    

        [HttpGet]
        [Authorize(Roles = "admin,faculty,group")]
    public ActionResult Students(int? groupId)
        {
            
            if (User.IsInRole("admin"))
            {
                if (groupId == null)
                {
                    var students = db.Students.Include(p => p.Group)
                                            .Include(p => p.Group.Faculty);

                    ViewBag.Students = students;

                    return View();
                }
                else
                {
                    var students = db.Students.Where(p => p.GroupId == groupId)
                                              .Include(p => p.Group)
                                              .Include(P => P.Group.Faculty);

                    ViewBag.Students = students;

                    return View();
                }
            }

            if (User.IsInRole("faculty"))
            {
                if (groupId == null)
                {
                    var user = db.Users.Find(User.Identity.GetUserId());

                    var students = db.Students.Where(p => p.Group.FacultyId == user.FacultyId)
                                                        .Include(p => p.Group)
                                                        .Include(p => p.Group.Faculty);

                    ViewBag.Students = students;

                    return View();
                }
                else
                {
                    var students = db.Students.Where(p => p.GroupId == groupId)
                                                        .Include(p => p.Group)
                                                        .Include(p => p.Group.Faculty);

                    ViewBag.Students = students;

                    return View();
                }
                
            }

            if (User.IsInRole("group"))
            {
                    var user = db.Users.Find(User.Identity.GetUserId());

                    var students = db.Students.Where(p => p.GroupId == user.GroupId)
                                                        .Include(p => p.Group)
                                                        .Include(p => p.Group.Faculty);

                ViewBag.Students = students;

                    return View();
            }


            //if (groupId == null & User.IsInRole("admin"))
            //{
            //    var students = db.Students.Include(p => p.Group)
            //                                .Include(p => p.Group.Faculty);

            //    ViewBag.Students = students;

            //    return View();
            //}



            //if (groupId != null & User.IsInRole("group"))
            //{
            //    var user = db.Users.Find(User.Identity.GetUserId());


            //    var students = db.Students.Include(p => p.Group)
            //                                .Include(p => p.Group.Faculty);

            //    ViewBag.Students = students;

            //    return View();
            //}

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin,group")]
        public ActionResult Students(Student student)
        {
            db.Students.Add(student);
            db.SaveChanges();

            return RedirectToAction("Students");
        }

        //[HttpGet]
        //public ActionResult Disciplines()
        //{
        //    var disciplines = db.Disciplines;

        //    ViewBag.Disciplines = disciplines;

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Disciplines(Discipline discipline)
        //{
        //    db.Disciplines.Add(discipline);
        //    db.SaveChanges();

        //    return RedirectToAction("Disciplines");
        //}

        public ActionResult Test()
        {
            ViewBag.Role = User.IsInRole("faculty");

            return View();
        }
    }
}